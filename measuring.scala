import java.sql.DriverManager
import java.sql.Connection
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

import org.apache.log4j.Logger
import org.apache.log4j.Level

Logger.getLogger("org").setLevel(Level.ERROR)
Logger.getLogger("akka").setLevel(Level.ERROR)

val sqlContext = new org.apache.spark.sql.SQLContext(sc)

val jdbcHostname = "localhost"
val jdbcPort = 5433
val jdbcDatabase ="digg"

// Create the JDBC URL without passing in the user and password parameters.
val jdbcUrl = s"jdbc:postgresql://${jdbcHostname}:${jdbcPort}/${jdbcDatabase}?user=postgres&password=ulysses"

// Create a Properties() object to hold the parameters.
import java.util.Properties
val connectionProperties = new Properties()

connectionProperties.put("postgres", s"${jdbcUsername}")
connectionProperties.put("ulysses", s"${jdbcPassword}")

val driverClass = "org.postgresql.Driver"
val connectionProperties = new java.util.Properties()
connectionProperties.setProperty("Driver", driverClass)

// users
var pushdown_query = "(SELECT DISTINCT voter_id FROM votes) votes"
val distinctUsers = spark.read.jdbc(url=jdbcUrl, table=pushdown_query, properties=connectionProperties)

// total_targets
pushdown_query = "(SELECT friend_id, count(*) FROM friends WHERE friend_id IN (SELECT DISTINCT voter_id FROM votes) GROUP BY friend_id) friends"
val userDegrees = spark.read.jdbc(url=jdbcUrl, table=pushdown_query, properties=connectionProperties)

// tv
pushdown_query = "(SELECT voter_id, count(*) FROM votes GROUP BY voter_id) votes"
val totalVotes = spark.read.jdbc(url=jdbcUrl, table=pushdown_query, properties=connectionProperties)

// weights  -- no wt yet
//pushdown_query = "(SELECT user_id, friend_id, COUNT(*) FROM votes a, votes b, friends c WHERE a.story_id = b.story_id AND a.voter_id = user_id AND b.voter_id = friend_id AND a.date > b.date AND a.date > c.date GROUP BY user_id, friend_id) votes"
//val weights = spark.read.jdbc(url=jdbcUrl, table=pushdown_query, properties=connectionProperties)
//weights.write.parquet("weightsdf")
val weights = sqlContext.read.parquet("weightsdf")

// friends_positions
//pushdown_query = "(SELECT user_id, friend_id FROM friends WHERE friend_id IN (SELECT distinct voter_id FROM votes) ORDER BY user_id) friends"
//val friends = spark.read.jdbc(url=jdbcUrl, table=pushdown_query, properties=connectionProperties)
//friends.write.parquet("friendsdf")
val friends = sqlContext.read.parquet("friendsdf")


var story_id = 1123

// sigma
pushdown_query = String.format("(SELECT DISTINCT voter_id FROM votes WHERE story_id = %d AND voter_id NOT IN (SELECT DISTINCT a.voter_id FROM votes a, votes b, friends c WHERE a.story_id = %d AND a.story_id = b.story_id AND a.voter_id = user_id AND b.voter_id = friend_id AND a.date > b.date AND a.date > c.date)) votes", story_id.asInstanceOf[Object], story_id.asInstanceOf[Object])
val sigma = spark.read.jdbc(url=jdbcUrl, table=pushdown_query, properties=connectionProperties)

// final_votes -- no final yet
pushdown_query = String.format("(SELECT DISTINCT voter_id FROM votes WHERE story_id = %d) votes", story_id.asInstanceOf[Object])
val final_votes = spark.read.jdbc(url=jdbcUrl, table=pushdown_query, properties=connectionProperties)

// story_infuence
pushdown_query = String.format("(select friend_id, count(*) from votes a, votes b, friends c where a.story_id = %d and b.story_id = a.story_id and a.voter_id = user_id and b.voter_id = friend_id and a.date > b.date and a.date > c.date group by friend_id) votes", story_id.asInstanceOf[Object])
val userStoryInfluence = spark.read.jdbc(url=jdbcUrl, table=pushdown_query, properties=connectionProperties)


val users: RDD[(VertexId, (Double, Double))] = distinctUsers.join(sigma, distinctUsers("voter_id") === sigma("voter_id"), "left_outer").
  rdd.map(x => (x(0).asInstanceOf[Number].longValue, ((if (x(1)==null) 0 else 1), (if (x(1)==null) 0 else 1))))

val relationships: RDD[Edge[Double]] = friends.join(weights, friends("user_id") === weights("user_id") && friends("friend_id") === weights("friend_id"), "left_outer").
     join(totalVotes, friends("friend_id") === totalVotes("voter_id") , "left_outer").
     join(userStoryInfluence, friends("friend_id") === userStoryInfluence("friend_id"), "left_outer").
     join(userDegrees, friends("friend_id") === userDegrees("friend_id"), "left_outer").
  rdd.map(x => Edge(x(0).asInstanceOf[Number].longValue, x(1).asInstanceOf[Number].longValue, (if (x(4)==null || x(8)==null) 0 else ((x(4).asInstanceOf[Number].floatValue / x(6).asInstanceOf[Number].floatValue) * (x(8).asInstanceOf[Number].floatValue / x(10).asInstanceOf[Number].floatValue)))))

var prevGraph: Graph[(Double, Double), Double] = null

var graph = Graph(users, relationships)

var diff = 0D

do {
  val newValues: VertexRDD[(Double, Double)] = graph.aggregateMessages[(Double, Double)](
    et => { // send a_ij*b_j , a_ij*b_j*z_j
      et.sendToSrc((et.attr * et.dstAttr._1, et.attr))
    },
    // sum
    (a, b) => (a._1 + b._1, a._2 + b._2)
  )

  //println(newValues.filter(_._1 == 491).first)

  val newUsers: RDD[(VertexId, (Double, Double))] = graph.vertices.leftOuterJoin(newValues).map(x => (x._1, ((x._2._1._2 + x._2._2.getOrElse(0D,0D).asInstanceOf[(Double,Double)]._1) / (1 + x._2._2.getOrElse(0D,0D).asInstanceOf[(Double,Double)]._2), x._2._1._2)))

  prevGraph = graph

  graph = Graph(newUsers, relationships)

  //newUsers.filter(x => x._2._1>0).filter(x => x._2._1<1).sortBy(_._1).take(20).foreach(println)

  diff = prevGraph.vertices.join(graph.vertices).map(x => Math.abs(x._2._1._1 - x._2._2._1)).reduce(_ + _)
  println("Diff: " + diff.toString)
}
while (diff > 0.5);

if (graph.vertices.filter(x => x._2._1==0).filter(x => x._2._2 > 0).count > 0) {
  println("ERRORRRRRR!")
}
graph.vertices.filter(x => x._2._1>0).coalesce(1).saveAsTextFile("storiesResults/" + story_id.toString)


//graph.vertices.filter(x => x._2._1>0).filter(x => x._2._1<1).sortBy(_._1).take(20).foreach(println)

//var a = friends.join(weights, friends("user_id") === weights("user_id") && friends("friend_id") === weights("friend_id"), "left_outer").
//     join(totalVotes, friends("friend_id") === totalVotes("voter_id") , "left_outer").
//     join(userStoryInfluence, friends("friend_id") === userStoryInfluence("friend_id"), "left_outer").
//     join(userDegrees, friends("friend_id") === userDegrees("friend_id"), "left_outer")

