#!/usr/bin/python
from __future__ import division
import numpy as np
import psycopg2
import random
import pickle
import csv

# global database connection settings
conn_string = "host='localhost' port='5433' dbname='digg' user='postgres' password='p@$$w0rd'"
conn = psycopg2.connect(conn_string)
cursor = conn.cursor()


# returns a list of friends for the given user
def get_friends_of(user):
    cursor.execute("SELECT distinct friend_id FROM friends WHERE user_id = %(user_id)s", {'user_id': user})
    rec_friends = cursor.fetchall()
    friends = []
    for (friend,) in rec_friends:
        friends.append(friend)
    return friends


# returns the count of friends of the given user that digg the given story
def get_count_of_friends(user, story):
    cursor.execute("SELECT DISTINCT friend_id FROM friends, votes WHERE user_id = %(user_id)s AND story_id = %(story_id)s AND voter_id = friend_id", {'user_id': user, "story_id": story})
    return cursor.rowcount


def get_weight(user, friend, weights, wt):
	weight = 0.0
		
	for (f, c) in weights[user]:
		if f == friend:
			weight = c / wt[f]
	return weight
	

def get_scaled(array, high = 1, low = 0):
	min_val = np.amin(array)
	max_val = np.amax(array)
	for x in xrange(np.size(array)):
		val = array[x]
		array[x] = low + (val - min_val) * (high - low) / (max_val - min_val)
	return array

# get all active network users
cursor.execute("SELECT DISTINCT voter_id FROM votes")
users = [item[0] for item in cursor.fetchall()]
print "Retrieved users"        

# get all total potential targets of each user
#total_targets = np.ones(len(users))
#cursor.execute("SELECT friend_id, count(*) FROM friends WHERE friend_id IN (SELECT DISTINCT voter_id FROM votes) GROUP BY friend_id")
#records = cursor.fetchall()
#for (user, count) in records:
#	total_targets[users.index(user)] = count
#with open('targets','w') as f:
#    pickle.dump(total_targets, f)

# load previously stored weights array
total_targets = pickle.load(open('targets', 'rb'))
print "Retrieved potential targets/friends"

# create an array for story story_id

story_id = 1123
#story_id = 3305
#story_id = 1462
#story_id = 1140

print "Starting for story: ", story_id

# query for all users activated in the end
cursor.execute("SELECT DISTINCT voter_id FROM votes WHERE story_id = %(story_id)s", {'story_id': story_id})
final_votes = [item[0] for item in cursor.fetchall()]
final = np.zeros(len(users))
for i, user in enumerate(users):
    if user in final_votes:
        final[i] = 1

# query for the count of votes of all users
#cursor.execute("SELECT voter_id, count(*) FROM votes GROUP BY voter_id")
#records = cursor.fetchall()
#tv = np.zeros(len(users))
#for (user, count) in records:
#	tv[users.index(user)] = count
#with open('tv','w') as f:
#    pickle.dump(tv,f)

# load previously stored friends_position array
tv = pickle.load(open('tv', 'rb'))

# query for all users activated without influence
cursor.execute("SELECT DISTINCT voter_id FROM votes WHERE story_id = %(story_id)s AND voter_id NOT IN (SELECT DISTINCT a.voter_id FROM votes a, votes b, friends c WHERE a.story_id = %(story_id)s AND a.story_id = b.story_id AND a.voter_id = user_id AND b.voter_id = friend_id AND a.date > b.date AND a.date > c.date)", {'story_id': story_id})
votes = [item[0] for item in cursor.fetchall()]
# initialize Sigma_i
sigma = np.zeros(len(users))
for i, user in enumerate(users):
    if user in votes:
        sigma[i] = 1
# verify count
print np.count_nonzero(sigma)
print "Retrieved sigma"

# create an array with friends positions
#friends_positions = []
#for i in xrange(len(users)):
#    friends_positions.append([])
#cursor.execute("SELECT user_id, friend_id FROM friends WHERE friend_id IN (SELECT distinct voter_id FROM votes) ORDER BY user_id")
#records = cursor.fetchall()
#for (user, friend) in records:
#    friends_positions[users.index(user)].append(users.index(friend))
#with open('friends_positions','w') as f:
#    pickle.dump(friends_positions,f)

# load previously stored friends_position array
friends_positions = pickle.load(open('friends_positions', 'rb'))

# create an array with weights
#weights = []
#for i in xrange(len(users)):
#    weights.append([])
#cursor.execute("SELECT user_id, friend_id, COUNT(*) FROM votes a, votes b, friends c WHERE a.story_id = b.story_id AND a.voter_id = user_id AND b.voter_id = friend_id AND a.date > b.date AND a.date > c.date GROUP BY user_id, friend_id")
#records = cursor.fetchall()
#for (user, friend, count) in records:
#    weights[users.index(user)].append((users.index(friend), count))
#with open('weights','w') as f:
#    pickle.dump(weights, f)

# load previously stored weights array
weights = pickle.load(open('weights', 'rb'))

wt = np.ones(len(users))
for i, weight in enumerate(weights):
	wt[i] = max(1, sum([y for (x, y) in weight]))


# create an array with infuence of user in story
story_infuence = np.zeros(len(users))
cursor.execute("select friend_id, count(*) from votes a, votes b, friends c where a.story_id = %(story_id)s and b.story_id = a.story_id and a.voter_id = user_id and b.voter_id = friend_id and a.date > b.date and a.date > c.date group by friend_id", {'story_id': story_id})
records = cursor.fetchall()
for (user, count) in records:
    story_infuence[users.index(user)] = count
    
ajq = story_infuence / total_targets
# scaling in the range [0, 9] was found to consistently work well
aj = get_scaled(ajq,9)

# create initial z array
z_t = np.zeros(len(users))

# code for neural network input files
#with open('story1123.csv', 'w') as csvfile:
#    fieldnames = ['intrinsic', 'aijsum', 'bjsum', 'label']
#    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
#    writer.writeheader()

for i, user in enumerate(users):
# unweighted model
#    z_t[i] = sigma[i]/(1 + sum([1 for x in friends_positions[i]]))
# weighte model
    z_t[i] = sigma[i]/(1 + sum([get_weight(i, x, weights, tv)*aj[x] for x in friends_positions[i]]))    
# code for neural network input files	
#        writer.writerow({'intrinsic': sigma[i], 'aijsum': sum([ajq[x] for x in friends_positions[i]]), 'bjsum': sum([get_weight(i, x, weights, tv) for x in friends_positions[i]]), 'label': ('TRUE' if final[i] > 0 else 'FALSE')})

z_t1 = np.zeros(len(users))
# start updating
while True:
    for i, user in enumerate(users):
# unweighted model	
#        z_t1[i] = (sigma[i] + sum([z_t[x] for x in friends_positions[i]]) )/(1 + sum([1 for x in friends_positions[i]]))
# weighted model
        z_t1[i] = (sigma[i] + sum([get_weight(i, x, weights, tv)*aj[x] * z_t[x] for x in friends_positions[i]]) ) / (1 + sum([get_weight(i, x, weights, tv)*aj[x] for x in friends_positions[i]]))
    print sum([abs(x) for x in (z_t1 - z_t)])
	# if converges, break
    if sum([abs(x) for x in (z_t1 - z_t)]) < 0.5:
		break
    for j in xrange(z_t.size):
        z_t[j] = z_t1[j]


for threshold in [x*0.005 for x in xrange(1,101)]:
	false_positive = 0    # non-voters that were identified as voters
	false_negative = 0    # voters that were identified as non-voters
	true_positive = 0     # voters that were identified as voters
	true_negative = 0     # non-voters that were identified as non-voters
	for i, user in enumerate(users):
		if z_t[i] >= threshold and final[i] == 1:
			true_positive += 1
		elif z_t[i] >= threshold and final[i] == 0:
			false_positive += 1
		elif z_t[i] < threshold and final[i] == 1:
			false_negative += 1
		else:
			true_negative +=1	
			
	precision = true_positive / (true_positive + false_positive)
	recall = true_positive / (true_positive + false_negative)
	print threshold, "\t", precision, "\t", recall, "\t", true_positive, "\t", false_negative, "\t", false_positive
