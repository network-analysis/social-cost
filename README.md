On the impact of social cost in opinion dynamics
=====

The implementation of the model discussed in: P. Liakos and K. Papakonstantinopoulou. **On the impact of social cost in opinion dynamics**. In ICWSM, 2016.

### Who do I talk to? ###

* [Panagiotios Liakos](http://www.di.uoa.gr/~p.liakos)
* [Katia Papakonstantinopoulou](http://www.di.uoa.gr/~katia)

Visit our [group](http://hive.di.uoa.gr/network-analysis) for more details.